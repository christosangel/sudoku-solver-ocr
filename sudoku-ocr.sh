#! /bin/bash
echo -e "  \033[1;33m┏━┓╻ ╻╺┳┓┏━┓╻┏ ╻ ╻   ┏━┓┏━┓╻  ╻ ╻┏━╸┏━┓"
echo -e "  ┗━┓┃ ┃ ┃┃┃ ┃┣┻┓┃ ┃   ┗━┓┃ ┃┃  ┃┏┛┣╸ ┣┳┛"
echo -e "  ┗━┛┗━┛╺┻┛┗━┛╹ ╹┗━┛   ┗━┛┗━┛┗━╸┗┛ ┗━╸╹┗╸`tput sgr0`\n\n"
input="$(zenity --file-selection --title="Select sudoku png to solve" --filename=$HOME/Desktop/)"
    case $? in
     0)
     ;;
     1)  exit
     ;;
    esac
convert "$input" -resize 900x900 810.png
line=1
column=1
echo "Optical Character Recognition:"
while [ $line -le 9 ]
do
	while [ $column -le 9 ]
	do
	echo -ne "Square $line$column\r"
	x=$(($line * 100 - 100 +10))
	y=$(($column * 100 - 100 +10))
	convert -crop 70x70+$y+$x 810.png tmp1.png
	convert number_label tmp1.png +append "$line""$column".png
	tesseract  --dpi 70 --psm 7 "$line""$column".png "$line""$column" >/dev/null 2>&1
	cat "$line""$column".txt |awk '{print $2}'|sed 's/[a-z]//g;s/[A-Z]//g;s/[[:punct:]]//g'>"$line""$column"
	if [ "$(awk '{print $1'} "$line""$column")" == "" ]; then echo " ">"$line""$column";fi
		((column++))
	done
	column=1
	((line++))
done
	echo -e "\nOptical recognition complete."
rm *.txt *.png
./sudoku-solver
rm {11..19} {21..29} {31..39} {41..49} {51..59} {61..69} {71..79} {81..89} {91..99}
