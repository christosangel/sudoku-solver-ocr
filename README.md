# SUDOKU SOLVER (Optical Recognition)
This project comes as an improvement of a previous one.

[https://gitlab.com/christosangel/sudoku-solver](https://gitlab.com/christosangel/sudoku-solver)

The main difference of this project with the previous one is that in this project there is a bash script that performs **Optical Character recognition (OCR)**.

 Thus the sudoku data are **not entered manually** by the user.

 Instead, the user **selects a png file of the puzzle (screenshot)**.

 You can see the script in action in [this video.](https://youtu.be/5O5NaGX7Z9E)

## DEPENDENCIES

The main dependency is [tesseract OCR](https://github.com/tesseract-ocr/tesseract), responsible for the optical character recognition.

Another important dependency is [imagemagick](https://imagemagick.org/index.php), responsible for the necessary image manipulation.

The file selection of the png image is done with [zenity](https://help.gnome.org/users/zenity/stable/).

To install these dependencies, run :

    sudo apt install tesseract-ocr imagemagick zenity

---
## INSTALL

Download all the respective files with

    git clone https://gitlab.com/christosangel/sudoku-solver-ocr
    cd sudoku-solver-ocr

Compile the c file, and make the bash script executable:

    gcc sudoku-solver.c -Wall -o sudoku-solver
    chmod +x sudoku-ocr.sh

After that, you are ready to go.



---
### Taking a screenshot

The user can use one of the many applications available in Linux to take a screenshot of the Puzzle to solve.

The user has to select the area of the puzzle **leaving out the outside frame of the puzzle**, and save it in a directory e.g. ~/Desktop/.

![0.png](png/0.png)

---
## USAGE

From the same directory,run the bash script:

    ./sudoku-ocr.sh



 run the script:

* You will be prompted to select a png file of a sudoku puzzle (that you want to solve).

![1.png](png/1.png)

* When the ocr procedure is complete, the user will be prompted to **check, correct or fill in any missing or mis-recognised numbers of the puzzle**.

![2.png](png/2.png)

* When the data is checked to be correct, the user hits **e** and **Enter**.

![3.png](png/3.png)

**Puzzle Solved!**

Solving the sudoku riddle can take from  a fraction of  a second to a couple of minutes, depending on

* the difficulty of the riddle and
* the CPU's calculating speed.
